module.exports = is_empty;

/* Test if an object is 'empty'.
 *
 * This is for using with plain objects/arrays.
 * This is only interested in objects/arrays.
 * This is only interested in own properties.
 */
function is_empty(obj) {
    if (typeof obj != 'object') {
        throw new Error("is_empty cannot give valid results on non-objects");
    }
    var property;
    for (property in obj) {
        if (obj.hasOwnProperty(property)) {
            return false;
        }
    }
    return true;
}
