var assert = require("assert");

var is_empty = require("../is_empty");

describe('is_empty', function(){
    it('returns true for an empty object', function() {
        assert.equal(is_empty({}), true);
    });
    it('returns false for a filled object', function() {
        assert.equal(is_empty({a: 'b', c: 'd'}), false);
    });
    it('returns false for an object with manually set length=0', function() {
        assert.equal(is_empty({length: 0}), false);
    });
    it('returns true for an empty array, err, object', function() {
        assert.equal(is_empty([]), true);
    });
    it('returns false for an array with ANY stuff', function() {
        assert.equal(is_empty([1, 2]), false);
        assert.equal(is_empty([0]), false);
        assert.equal(is_empty([""]), false);
        assert.equal(is_empty([null]), false);
        assert.equal(is_empty([undefined]), false);
    });
});
